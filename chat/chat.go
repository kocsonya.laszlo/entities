package chat

import "time"

type ActionType string

const (
	ActionTypeMessage ActionType = "message"
	ActionTypeJoin    ActionType = "join"
)

type SentByType string

const (
	SentByTypeCustomerContact SentByType = "customerContact"
	SentByTypeAgent           SentByType = "agent"
)

type HistoryKind string

const (
	HistoryKindEdit   HistoryKind = "edit"
	HistoryKindDelete HistoryKind = "delete"
)

type ChatLog struct {
	Tenant           string           `bson:"tenant"`
	Uuid             string           `bson:"_id"`
	ConversationUuid string           `bson:"conversationUuid"`
	StartedAt        time.Time        `bson:"startedAt"`
	EndedAt          time.Time        `bson:"endedAt"`
	ContactMetadata  *ContactMetadata `bson:"contactMetadata"`
	Actions          []*Action        `bson:"actions"`
}

type ContactMetadata struct {
	Browser        string `bson:"browser"`
	Language       string `bson:"language"`
	CustomerSearch string `bson:"customerSearch"`
}

type Action struct {
	Uuid       string         `bson:"uuid"`
	Type       ActionType     `bson:"type"`
	CreatedAt  time.Time      `bson:"createdAt"`
	SentByType SentByType     `bson:"sentByType"`
	SentById   string         `bson:"sentById"`
	Message    *MessageAction `bson:"message"`
	Join       *JoinAction    `bson:"join"`
}

type MessageAction struct {
	Message string                `bson:"message"`
	History []*MessageHistoryItem `bson:"history"`
}

type MessageHistoryItem struct {
	Kind            HistoryKind `bson:"kind"`
	ChangedAt       time.Time   `bson:"changedAt"`
	PreviousMessage string      `bson:"previousMessage"`
}

type JoinAction struct {
	Type string `bson:"type"`
	Uuid string `bson:"uuid"`
}
